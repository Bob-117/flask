# Flask - Marshmallow - SQLAlchemy - Flasgger / Swagger

Etude et utilisation des solutions flassger/swagger et Marshmallow dans un but de documentaion efficace d'un projet type Api flask/python/docker.
<br>
Utilisation de SQLAlchemy qui profite également des schemas implémentés par Marshmallow.
<br>
Ce rapport est rédigé en français. Néanmoins, de nombreux termes anglophones sont conservés afin de permettre au lecteur de faire le lien avec les ressources existantes, notamment lorsque la traduction n’est pas clairement définie.

### Acces base de donnees :
```shell
docker run -it -p 5432:5432 -e  POSTGRES_PASSWORD=password -e POSTGRES_USER=user nexus.tasa.fr:8097/postgres:14

psql -h localhost -p 5432  -U user --password
```

## DESCRIPTION
```mermaid
%%{ init : { "theme" : "base"}}%%
flowchart TB
    subgraph HOST
        subgraph App
        
            subgraph API
                Main
                Route
            end
            
            subgraph Marshmallow
                Schema
                Service
                Model
            end
            subgraph ORM
                SQLAlchemy
            end
        end
        
        subgraph database
            docker_database
        end
        
        subgraph doc
            flasggerdoc
        end
    end
    
    Route --- Schema
    Schema --- Model 
    Schema --- Service --- Model
    Route --- |"@swag_from"| doc
    Model --- ORM
    ORM --- database
    Main --- |register flask blueprint| Route
    Marshmallow -.- doc

```

Ce projet a pour but d'étudier la solution swagger pour la génération de documentation d'api. Nous définissons un contexte simple afin de réaliser cet état de l'art : 
<br> La gestion d'items ayant une catégorie (traitées avec des "products" puis mise de coté pour le moment), et de commandes (order) composées d'une liste (items) et d'un client. La gestion des categories et des items sont idépendantes a l'heure actuelle.
<br> Des parties de code sont en commentaire / non importées et donc non fonctionnelles. Cependant elles restent de potentiels éléments d'appréhension et de compréhension de l'utilisation de la documentation swagger.
<br> Les fichiers et parties de code concernant les "Products" fait partie du code mis a part, mais la gestion d'une filiation entre Product et Category est une premiere approche de la mise en place de schema marshmallow avec heritage que nous verrons plus tard.

```mermaid
classDiagram
  Category "1" *-- "0..*" Item 
  Order "0..*" --* "0..*" Item
  Order "0..*" --* "1" Customer 
  Customer: order()
  Customer: String name
  Customer: Datetime last_order
  Customer: Float total_price
  Order: close()
  Order: Customer customer
  Order: Item[] items
  Order: Float total_price
  Item: String name
  Item: Category category
  Item: Float price
  Category: String name
```

## UTILISATION :

```shell
git clone git@gitlab.tasa.fr:obricaud/flasgger_sqlalchemy_marshmallow.git
cd flasgger_sqlalchemy_marshmallow/
(git checkout)
pip install -r requirements.txt
python3 main.py
```

### curl 127.0.0.1/5000/item/list

```yaml
[
    {
        "id": 1,
        "name": "gibson sg",
        "price": 1200.0
    },
    {
        "id": 2,
        "name": "fender telecaster",
        "price": 1000.0
    },
    {
        "id": 3,
        "name": "gibson explorer",
        "price": 314159265.0
    },
    {
        "id": 4,
        "name": "test",
        "price": 117.0
    }
]

```

## ELEMENTS D'ARCHITECTURE 


- Les modèles (*models*) sont les descriptions des données de notre application, majoritairement liés à la base de données.

- Les routes (*routes*) sont les URIs de notre application, là où sont définies nos ressources et nos actions.

- Les schémas (*schemas*) sont les définitions des entrants et sortants (*inputs & outputs*) de notre API, quelles infos sont autorisées ainsi que les résultats renvoyés. Ils sont corrélés à nos ressources mais pas forcément à nos modèles.

- Les services (*services*) sont des modules qui définissent la logique applicative et/ou qui intéragissent avec d'autres services (ie: la couche db), les routes devraient être les plus simples possible et déléguer toute l'intelligence à ce niveau.<br></br>


### Avec :
- [x] ma = Marshmallow()
- [X] db = SQLAlchemy()
- [X] from sqlalchemy.ext.hybrid import hybrid_property
- [ ] [Documentation Swagger](##documentation-flasgger-swagger-)
- [ ] [Guide generique](##guide-)


<details>
<summary> Exemple de certains éléments fonctionnels concernant les catégories de produit </summary>

<table style="font-size: xx-small">
    <tr>
        <td><h2>#</h2></td>
        <td><h2>Route</h2></td>
        <td><h2>Model</h2></td>
        <td><h2>Schema</h2></td>
        <td><h2>Service</h2></td>
    </tr>
    <tr>

<td>
<h2>GET</h2>
</td>
<td>

```python
@category_api.route("/list", methods=["GET"])
@swag_from({
    "tags": ["category"],
    "summary": "Full category list",
    "description": "Get full category list from db",
    "responses": {
        200: {
            "description": "category data",
            "schema": CategorySchema
        }
    }
})
def get_category():
  """
  Return all category from database
  """
    try:
        category_list_model = CategoryModel.query.all()
        category_list_schema = CategorySchema(many=True)
        return category_list_schema.dump(category_list_model)
    except Exception as e:
        return ErrorGetSchema.dump(e)

```
<td>

```python
class CategoryModel(db.Model):
    """ 
    class CategoryModel inherits from SQLAlchemy Model
    """
    __tablename__ = "category"
    id = db.Column("id", db.Integer, primary_key=True, autoincrement=True)
    __name = db.Column("name", db.Text, nullable=False, default="")

    def __init__(self, name):
        self.name = name

    @hybrid_property
    def name(self):
        return self.__name

    @name.setter
    def name(self, new_name):
        self.__name = new_name

# end class CategoryModel
```

</td>

<td>

```python
class CategorySchema(ma.SQLAlchemyAutoSchema):
    """
    Classe CategorySchema inherits from marshmallow schema
    """

    class Meta:
        """
        Classe Meta de CategorySchema
        """
        # Champs à exposer
        model = CategoryModel
    # end class Meta
# end class CategorySchema

```
</td>

<td>

```python
class CategoryService:
    """
    Check if a category given name already exists in database
    """
    @staticmethod
    def does_exist(search_name):
        try:
            return CategoryModel.query.filter_by(name=search_name).first() is None
        except Exception as e:
            return e

```
</td>

</tr>
    <tr>
<td> <h2>POST</h2> </td>

<td>

```python
@category_api.route("/add", methods=["POST"])
@swag_from({
    "summary": "add a new category",
    "description": "add a new category in db with given name",
    "tags": ["category"],
    "parameters": [
        {
            "description": "Create a new product & save it to db",
            "in": "body",
            "schema": CategorySchemaCustom,
            "example": "drums"
        }
    ],
    "responses": {
        200: {
            "description": "Category creation info",
            "schema": CategoryCreationSuccessSchema
        },
        400: {
            "description": "Category creation error",
            "schema": CategoryCreationFailureSchema
        }
    }
})
def add_category():
    new_category_name = request.get_json()["name"]

    if CategoryService.does_exist(new_category_name):
        new_cat_model = CategoryModel(name=new_category_name)
        db.session.add(new_cat_model)
        db.session.flush()
        db.session.commit()
        data_status = 201
        data_msg = "creation success"
        data_new_created = new_cat_model.id
    # end if

    else:
        data_status = 409
        data_msg = "conflict, already exists"
        data_new_created = -1

    output_schema = CategoryCreationSuccessSchema()
    data = {
        "status": data_status,
        "msg": data_msg,
        "new_created": data_new_created
    }
    return output_schema.load(data)
```

</td>

<td>

```python
"""idem"""
```

</td>

<td>

```python
class CategorySchemaCustom(Schema):
    """
    class CategorySchemaCustom
    """

    class Meta:
        """
        Classe Meta de CategorySchemaCustom
        """
        # Champs à exposer
        fields = ["name"]

    # end class Meta
    name = Str(required=True,
               description="product category",
               example="guitar")

    #####################################################################
class CategoryCreationSuccessSchema(Schema):
    """
    class CategoryCreationSuccessSchema
    """

    class Meta:
        """
        Classe Meta de CategoryCreationSuccessSchema
        """
        # Champs à exposer
        fields = ["status", "msg", "new_created"]

    # end class Meta
    status = Integer(example=201)
    msg = Str(example="created")
    new_created = Integer(example=17)


    #####################################################################
class CategoryCreationFailureSchema(Schema):
    """
    CategoryCreationFailureSchema
    """

    class Meta:
        """
        Classe Meta de CategoryCreationFailureSchema
        """
        # Champs à exposer
        fields = ["status", "msg", "new_created"]

    # end class Meta
    status = Integer(example=409)
    msg = Str(example="conflict, already exists")
    new_created = Integer(example=-1)
```

</td>

<td>

```python
"""idem"""
```

</td>

</tr>
</table>

</details>

## DOCUMENTATION FLASGGER SWAGGER :

Une fois l'application lancée, la documentation flasgger est accessible sur http://127.0.0.1:5000/apidocs/
<br>Les spécifications définies sont disponibles au format json sur http://127.0.0.1:5000/apispec_1.json

L'utilisation de Swagger Editor (docker image disponible sur Nexus) permet de visualiser un exemple complet.

### Il existe plusieures facons d'utiliser et de generer la documentation swagger

- *Note : Flasgger est la lib python qui permet l'implementation de la doc en décorateur (voir plus bas), Swagger/UI est l'outil de visualisation (et d'exécution, parfois indisponible) des différentes spécifications des routes de notre API.*

<details>

<summary>
  - [ ] Définition directement dans les docstring des routes
</summary>

```python
@category_api.route("/add", methods=["POST"])
def add_category():
  """
  Methode de recuperation de la liste complete des clients
  ---
  parameters:
  - name: name
    description: nom de la nouvelle categorie a enregistrer
    required: True
    type: string
  :return: La liste des client avec un HTTP code 200.
  :rtype: list
  """
  try:
    pass
    # logic goes here
    # new_name = request.get_data() 
  except Exception as e:
    return e

```
</details>

<details>

<summary>
  - [ ] Utilisation d'un fichier externe (ici doc.yaml a la racine du projet)
</summary>

```python
from flasgger import swag_from
from flask import Blueprint

main_route_api = Blueprint("main_route_api", __name__)

@main_route_api.route("/", methods=["GET"])
@swag_from("../doc.yaml")
def home():
  return "home"

```

```yaml
swagger: "2.0"
info:
  description: "Generic yaml file to describe Swagger documentation"
  version: "1.0.0"
  title: "Swagger Documentation"
tags:
- name: "Category"
  description: "Access to category management"
- name: "Product :"
  description: " Access to products management"
- name: "Api Response :"
  description: " All api response"
paths:
  /category:
    get:
      tags:
        - "Category"
        summary: "get list with all category existing in db"
        description: "get full customers list"
        operationId: "getCategory"
        consumes:
        - "application/json"
        - "application/xml"
        produces:
        - "application/xml"
        - "application/json"
        parameters:
        - out: "body"
          name: "body"
          description: "category list from database || []"
          required: true
          schema:
            $ref: "#/definitions/Category"
        responses:
          "200":
            description: "Success"
          "500":
            description: "Failure"
    post:
      tags:
        - "Category"
        parameters:
          - in: "body"
            description: "data about new category"
            required: true
            schema:
              $ref: "#/definitions/CategoryCreationIn"
          - out: "body"
              schema:
                $ref: "#/definitions/CategoryCreationOut"
definitions:
  Category
              
```
</details>

<details>

<summary>
  - [ ] Spécification au moyen de décorateurs (@swag_from):
</summary>

```python
from flasgger import swag_from

@customer_api.route("/list", methods=["GET"])
@swag_from({
"parameters": [
    {
      "description": "Liste de tous les clients enregistres en db",
      "in": "body",
      "schema": GetCustomerListSchemaOutput
    }
],
"responses": {
    HTTPStatus.OK: {
      "description": "Retour de la liste des clients",
      "schema": "#TODO"
    }
}
})
def get_customer():
return {"name": "bob", "age": 117}

```

</details>


##### En approbation avec nos besoins, le choix d'utiliser la methode decorateur @swag_from est pertinent. Nous definissons des modeles et des schemas à la fois fonctionnels et utilisés dans le code, et sur lesquels swagger repose pour générer la documentation de notre API. <br> La simple modification, l'ajout ou la suppression d'un champs (fields) dans notre modele aura une influence directe sur la base de donnée. <br>L'utilisation des schemas marchmallow offre une evolution aisee de l'application. En fonction des besoins / des besoins client, l'ajout d'un nouveau champ (une date de creation d'un item par exemple) reside en l'ajout ou la modification d'une ligne de code dans notre modele et dans les differents schema qui en herite. Et cette modification serait repercutee directement dans la doc autogeneree swagger.
## GUIDE

<details>

<summary>
- Cas simple : 
<br> Une table Item indépendante avec pour colonne l'id (autoincrement, géré par la base de données), le nom (name VARCHAR) de l'item et son prix (price FLOAT) :
<br> Les actions concernant les items sont : La *consultation* de la liste des items enregistrés en base de données, ainsi que  la *création*, la *modification* et la *suppression* d'un item
<br><br>Nous pouvons donc définir différentes exigences et différents schemas adaptés a ces dernières. Les schemas nous permettent entre autre de filtrer les informations entrantes et sortantes en fonction des besoins et des conditions de vérification (gérées dans nos services principalement).
</summary>
Par exemple :


<table>

<tr>
<th>#</th>
<th>Route</th>
<th>Modeles associes</th>
<th>Schemas associes</th>
<th>Services associes</th>
</tr>


<tr>
<td>
<h3>Ajout d'un item</h3>
</td>
<td>

```python
@item_api.route("/add", methods=["POST"])
@swag_from({
    "summary": "add a new item",
    "description": "add a new item in db with given name & price",
    "tags": ["Item"],
    "parameters": [
        {
            "description": "Create a new item & save it to db",
            "in": "body",
            "schema": ItemSchemaCreationIn,
            "example": "{\"name\": \"gibson sg\", \"price\": \"1000\"}"
        }
    ],
    "responses": {
        200: {
            "description": "Item creation info",
            "schema": ItemSchemaCreationSuccess
        },
        400: {
            "description": "Item creation error",
            "schema": ItemSchemaCreationFailure
        }
    }
})
def add_item():
    new_item_name = request.get_json()["name"]
    new_item_price = request.get_json()["price"]

    if not ItemService.entry_check(name=new_item_name, price=new_item_price):
        return {"msg": "bad entry"}

    if ItemService.does_exist(new_item_name):
        new_item_model = ItemModel(name=new_item_name, price=new_item_price)
        db.session.add(new_item_model)
        db.session.flush()
        db.session.commit()
        data_status = 201
        data_msg = "creation success"
        data_new_created = new_item_model.id
    # end if

    else:
        data_status = 409
        data_msg = "conflict, already exists"
        data_new_created = -1

    output_schema = ItemSchemaCreationSuccess()
    data = {
        "status": data_status,
        "msg": data_msg,
        "new_created": data_new_created
    }
    return output_schema.load(data)
```
</td>
<td>

```python
class ItemModel(db.Model):
    __tablename__ = "item_table"
    id = db.Column("id", db.Integer, primary_key=True, autoincrement=True)
    name = db.Column("name", db.String)
    price = db.Column("price", db.Float)

    def __init__(self, name, price):
        self.name = name
        self.price = price
```
</td>
<td>

```python
class ItemSchema(ma.SQLAlchemyAutoSchema):
    """
    class ItemSchema
    """
    class Meta:
        model = ItemModel


class ItemSchemaCreationIn(Schema):
    model = ItemModel
    fields = ["name", "price"]
    name = Str()
    price = Float()


class ItemSchemaCreationSuccess(Schema):
    """
    class ItemSchemaCreationSuccess
    """

    class Meta:
        """
        Classe Meta de ItemSchemaCreationSuccess
        """
        # Champs à exposer
        fields = ["status", "msg", "new_created"]

    # end class Meta
    status = Integer(example=201)
    msg = Str(example="created")
    new_created = Integer(example=17)


class ItemSchemaCreationFailure(Schema):
    """
    ItemCreationFailureSchema
    """

    class Meta:
        """
        Classe Meta de ItemCreationFailureSchema
        """
        # Champs à exposer
        fields = ["status", "msg", "new_created"]

    # end class Meta
    status = Integer(example=409)
    msg = Str(example="conflict, already exists")
    new_created = Integer(example=-1)

```
</td>

<td>

```python
class ItemService:
    @staticmethod
    def does_exist(search_name):
        try:
            return ItemModel.query.filter_by(name=search_name).first() is None
        except Exception as e:
            return e

    @classmethod
    def entry_check(cls, name, price):
        return type(price) is float or type(price) is int and type(name) is str
```
</td>
</tr>


</table>

</details>

Nous implémentons des schemas précis des informations que nous souhaitons traiter dans des situations données : 
- Dans le cadre de la modification d'un item, un retour à l'utilisateur de cette modification avec l'ancienne et la nouvelle valeur du nom est pertinente
- Pour un ajout, seuls un nouveau nom et un nouveau prix sont nécessaires (et l'id n'existe pas encore a l'entrée puisqu'il sera auto généré en base de données, mais il peut ếtre interessant d'avoir une retour de ce nouvel id une fois la création validée)
- Nous pouvons adapter différents schemas de réponse dépendant de nos route de CRUD, avec un unique message lors d'erreur d'entrée ou de base de données, ou des informations plus detaillées lors du succès d'une operation.

<details>

<summary>
- Pour aller plus loin (many to many & table d'association):
</summary>

#### Gestion de commandes : objet commande avec comme attribut un customer (objetc cliet) et une liste d'objett item


```mermaid
erDiagram
    ORDER }|--|{ ORDER_HAS_ITEM : relation
    ORDER_HAS_ITEM }|--|{ ITEM : relation
    ORDER }|--|{ CUSTOMER : commande
    ORDER_HAS_ITEM{
      int order_id
      int item_id
    }
    ORDER {
      int id
      Customer customer
    }
    ITEM {
      int id
      string name
      float price
    }
    CUSTOMER {
      string name
    }
```

<table>

<tr><td>#</td><td>Route</td><td>MODEL</td><td>SCHEMA</td></tr>

<tr>

<td>
Get Orders List
</td>

<td>

```python
@order_api.route("/list", methods=["GET"])
@swag_from({
    "tags": ["ORDERS"],
    "summary": "Full category list",
    "description": "Get full category list from db",
    "responses": {
        200: {
            "description": "category data",
            "schema": OrderSchemaAuto
        },
        201: {
            "description": "okok",
            "schema": OrderSchemaListOut
        }
    }
})
def get_order():
    main_output_list = []
    try:
        order_list_model = OrderModel.query.all()
        for order in order_list_model:
            current_order_id = order.id
            current_order_items = []
            current_order_customer = order.customer

            current_customer_schema = CustomerSchema().load({"id": current_order_customer})

            clause = text(f"order_id = {order.id}")

            order_has_item_model = db.session.execute(
              order_has_item.join(ItemModel, ItemModel.id == order_has_item.c.item_id).select().where(clause)
            )

            for relation in order_has_item_model:
                # print(dict(relation))
                data = {
                    "id": relation.item_id,
                    "name": relation.name,
                    "price": relation.price
                }
                current_item_schema = ItemSchema().load(data=data)
                current_order_items.append(current_item_schema)
            final_data = {
                "customer": current_customer_schema,
                "items": current_order_items
            }

            main_output_list.append(OrderSchemaOut().load(final_data))
            # print(current_order_model.show())
        return main_output_list

    except Exception as e:
        logging.getLogger("get_order").error(e)
        return {"msg": e}
```

</td>

<td>

```python
order_has_item = Table(
    "order_has_item",
    db.metadata,
    db.Column("order_id", db.Integer, db.ForeignKey("order_table.id"), primary_key=True),
    db.Column("item_id", db.Integer, db.ForeignKey("item_table.id"), primary_key=True)
)


class OrderHasItemModel(Model):
    __tablename__ = "order_has_item"
    order_id = Integer()
    items_id = List(Integer())


# ITEM
class ItemModel(db.Model):
    __tablename__ = "item_table"
    id = db.Column("id", db.Integer, primary_key=True, autoincrement=True)
    name = db.Column("name", db.String)
    price = db.Column("price", db.Float)

    def __init__(self, name, price):
        self.name = name
        self.price = price


class OrderModel(db.Model):
    __tablename__ = "order_table"
    id = db.Column("id", db.Integer, primary_key=True, autoincrement=True)
    date = db.Column("date", db.DateTime)
    customer = db.Column("customer", db.Integer, db.ForeignKey("customer.id"))

    # items = relationship("ItemModel", back_populates="", secondary=order_has_item)

    def __init__(self, date, customer):
        self.customer = customer
        self.date = date
```

</td>

<td>

```python
class OrderSchemaAuto(ma.SQLAlchemyAutoSchema):
    """
    Classe CategorySchema
    """

    class Meta:
        """
        Classe Meta de CategorySchema
        """
        # Champs à exposer
        model = OrderModel
    # end class Meta
    
class OrderSchemaOut(Schema):
    fields = ["id", "customer", "items", "date"]
    id = Integer()
    customer = Nested(CustomerSchema)
    items = List(Nested(ItemSchema))
    date = DateTime()


class OrderSchemaListOut(Schema):
    fields = ["orders"]
    orders = List(Nested(OrderSchemaOut))


```
</td>

</tr>
</table>

##### Nous definissons ici differents schema avec des attributs "Nested" ie le schema d'une commande (order) est composé d'un schema de customer ainsi que d'une liste de schema d'item. L'utilisation des relationship de SQLAlchemy permettra une meilleure gestion des tables de jointure lors de grosses requetes SQL.
</details>
