# -*- coding: utf-8 -*-
import requests
import conf
path = "/item"

url = "http://" + conf.SERVER_IP + ":" + conf.SERVER_PORT + path

response = requests.get(url)
print(response.content)

new_item = {"new": "17", "brand": "gibson"}
post = requests.post(url, json=new_item)
print(post.content)
