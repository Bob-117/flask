from flask import Flask, request, jsonify

app = Flask(__name__)


@app.route("/item", methods=['GET'])
def get_all():
    return "get_all"


app.run(port=5000, host='127.0.0.1')
