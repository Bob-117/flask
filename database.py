import conf
import psycopg2
from psycopg2 import connect

import os


class Database:

    def __init__(self):
        self.__host = conf.DATABASE_HOST
        self.__port = conf.DATABASE_PORT

    def show(self):
        print('{0} :: {1}'.format(self.get_host(), self.get_port()))

    def get_host(self):
        return self.__host

    def get_port(self):
        return self.__port

    def get_connection(self):
        print("get conn")
        init_conn = psycopg2.connect(host=self.get_host(),
                                     port=self.get_port(),
                                     database='flask',
                                     user="flask",
                                     password="flask")
        return init_conn

    def select_query(self):
        print("query")
        conn = self.get_connection()
        cur = conn.cursor().execute('SELECT * FROM items;')
        items = cur.fetchall()
        cur.close()
        conn.close()
        return items

    def query_insertinto(self, data):
        print(data)
        print("INSERT INTO.prepared.execute on " + self.show())


# db = Database()
#
# db.show()
# print(22)

# conn = psycopg2.connect(
#     host="localhost",
#     database="flask_db",
#     user=os.environ['DB_USERNAME'],
#     password=os.environ['DB_PASSWORD'])

# Open a cursor to perform database operations
# cur = conn.cursor()

# def get_db_connection():
#     conn = psycopg2.connect(host='localhost',
#                             database='flask_db',
#                             user=os.environ['DB_USERNAME'],
#                             password=os.environ['DB_PASSWORD'])
#     return conn


# @app.route('/')
# def index():
#     conn = get_db_connection()
#     cur = conn.cursor()
#     cur.execute('SELECT * FROM books;')
#     books = cur.fetchall()
#     cur.close()
#     conn.close()
#     return render_template('index.html', books=books)


# conn = psycopg2.connect(host=db.get_host(),
#                         port=db.get_port(),
#                         database='flask',
#                         user="flask",
#                         password="flask")
# print(conn)


# conn = psycopg2.connect(host="127.0.0.1", port=53844 ,dbname="postgres", user="flask", password="flask")
# conn = psycopg2.connect(host="127.0.0.1", port=53844 ,dbname="postgres")
# cur = conn.cursor()
# cur.execute("CREATE TABLE test (id serial PRIMARY KEY, num integer, data varchar);")
# cur.execute("INSERT INTO test (num, data) VALUES (%s, %s)", (100, "abc'def"))
# cur.execute("SELECT * FROM test;")
# cur.fetchone()
# conn.commit()
# cur.close()
# conn.close()

conn = psycopg2.connect(
    host="localhost", port=53844,
    database="suppliers",
    user="postgres",
    password="Abcd1234")
print(conn)

# conn = connect(
#         database=database,
#         user=username,
#         password=password,
#         host=hostname,
#         port=port,
#         connect_timeout=3,
#         # https://www.postgresql.org/docs/9.3/libpq-connect.html
#         keepalives=1,
#         keepalives_idle=5,
#         keepalives_interval=2,
#         keepalives_count=2)
