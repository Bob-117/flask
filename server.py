import json
import os

import pathlib

from flask import Flask, request, jsonify

import conf

# import database
# from database import Database


class Server:

    def __init__(self):
        self.__ip = conf.SERVER_IP
        self.__port = conf.SERVER_PORT
        # self.__db = Database()

    def save(self):
        print("save")
        # print(self.__db.show())
        # self.__db.save(data)
############################################################################
        # entry = {
        #     "Name": "Person_3",
        #     "Age": 34,
        #     "Email": "33@gmail.com"
        # }
        #
        # fname = "save.json"
        # a = []
        # if not os.path.isfile("save.json"):
        #     a.append(entry)
        #     with open(fname, mode='w') as f:
        #         f.write(json.dumps(a, indent=2))
        # else:
        #     with open(fname) as feedsjson:
        #         feeds = json.load(feedsjson)
        #
        #     feeds.append(entry)
        #     with open(fname, mode='w') as f:
        #         f.write(json.dumps(feeds, indent=2))

    ############################################################################
        # filename = 'save.json'
        # path = str(pathlib.Path().resolve()) + filename
        # # # Check if file exists
        # # if path.isfile(filename) is False:
        # #     raise Exception("File not found")
        #
        # # Read JSON file
        # with open(filename) as currentdb:
        #     # listObj = json.load(fp)
        #     # listObj = currentdb.read()
        #     print("---------------------------------")
        #     print(type(currentdb))
        #     print(type(currentdb.read()))
        #     print(currentdb.read())
        #     print("---------------------------------")
        #     # # print(json.load(json.dumps(currentdb)))
        #     # print(json.load(currentdb))
        #     # print("---------------------------------")
        #     # print(type(currentdb.read()))
        #     # print("---------------------------------")
        #
        # listObj = []
        # # Verify existing list
        # # print(listObj)
        # #
        # # print(type(listObj))
        #
        # listObj.append({
        #     "Name": "Person_3",
        #     "Age": 34,
        #     "Email": "33@gmail.com"
        # })
        #
        # # Verify updated list
        # print(listObj)
        #
        # with open(filename, 'a') as json_file:
        #     json_file.write(json.dumps(listObj,
        #                indent=4,
        #                separators=(',', ': ')))
        #
        # print('Successfully appended to the JSON file')
    ############################################################################
    def start(self):
        print("start")
        app = Flask(__name__)

        @app.route("/item", methods=['GET'])
        def get_all():
            return "get_all"

        @app.route("/item", methods=['POST'])
        def post_one():
            data = request.data

            # UTF-8 bytes to Unicode + '' to ""
            my_json = data.decode('utf8').replace("'", '"')
            print("post")
            print(my_json)
            print(type(my_json))
            print("post")

            # json to list => dump formatted
            data = json.loads(my_json)
            s = json.dumps(data, indent=4, sort_keys=True)
            print("post2")
            print(s)
            print(type(s))
            print("post2")

            # path = pathlib.Path().resolve() + '/save.json'
            path = str(pathlib.Path().resolve()) + '/save.json'
            print("post3")
            print(path)
            print("post3")
            self.save()
            # with open(path, 'a') as a_writer:
            #     a_writer.write(s + ', \n')
            #
            # with open('data.json', 'w', encoding='utf-8') as f:
            #     json.dump(my_json, f, ensure_ascii=False, indent=4)
            return data
            # return "post_one"

        @app.route("/item", methods=['PUT'])
        def update_one():
            print(request.data)
            return "update_one"

        @app.route("/item", methods=['DELETE'])
        def delete_one():
            print(request.data)
            return "delete_one"

        # @app.route("/")
        # def hello():
        #     return "Hello, World!"

        @app.route("/item", methods=['GET', 'POST', 'PUT'])
        def match_case():
            print(request.method)
            # Logic
            return "True"

        app.run(port=self.__port, host=self.__ip)
